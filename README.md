# Docker Based Development scripts

This repository contains basic scripts to ease container based application development.

Those scripts will help in mounting containers containing project specific tools (toolchain, compilers ...) in project directories.

# install

To install those scripts simply run:
```bash
./install.sh
```

# docker hosts

Create a `.docker-builder` file containing the docker-image name.

Or create a `.docker-builder.sh` with the following content:
```bash
# options to pass to docker command
OPTS="--entrypoint=/bin/bash"
IMAGE="node:slim"
```

Then use the following commands:
```bash
# Spawn a shell
docker-builder

# Run a command
docker-builder ls
```

# ssh hosts

Create a `.ssh-builder` file containing the hostname to connect to.

Or create a `.ssh-builder.sh` with the following content:
```bash
RHOME="/remote/home/dir"
RHOST="remote-host"
```

Then use the following commands:
```bash
# Spawn a shell
ssh-builder

# Run a command
ssh-builder ls
```

To sync your work-directory on the remote host:
```bash
ssh-builder-sync

# To sync back files from the remote host
ssh-builder-sync --reverse ./src
```

# X hosts

A generic command that will spawn either docker or ssh related commands is available:
```bash
# will call the proper *-builder command
x-builder

# A shorthand for cquery
x-cquery
```
